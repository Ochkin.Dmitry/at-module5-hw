package junit.tests;

import junit.BaseTest;
import org.junit.Assert;
import org.junit.Test;

public class SubTest extends BaseTest {

  @Test
  public void subLongTest() {
    long actualResult = calculator.sub(2L, 1L);
    Assert.assertEquals(1L, actualResult);
  }

  @Test
  public void subDoubleTest() {
    double actualResult = calculator.sub(3.00, 1.00);
    Assert.assertEquals(2.00, actualResult, 0.01);
  }
}
