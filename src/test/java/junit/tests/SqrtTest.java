package junit.tests;

import junit.BaseTest;
import org.junit.Assert;
import org.junit.Test;

public class SqrtTest extends BaseTest {

  @Test
  public void sqrtTest() {
    double result = calculator.sqrt(4.00);
    Assert.assertEquals(2.00, result, 0.01);
  }
}
