package junit.tests;

import static org.junit.Assert.assertEquals;

import junit.BaseTest;
import org.junit.Test;

public class PowTest extends BaseTest {

  @Test
  public void powTest(){
    double result = calculator.pow(2.00, 2.00);
    assertEquals(4.00, result, 0.01);
  }
}
