package junit.tests;

import junit.BaseTest;
import org.junit.Assert;
import org.junit.Test;

public class MultTest extends BaseTest {

  @Test
  public void multLongTest() {
    long actualResult = calculator.mult(2L, 2L);
    Assert.assertEquals(4L, actualResult);
  }

  @Test
  public void multDoubleTest() {
    double actualResult = calculator.mult(2.00, 2.00);
    Assert.assertEquals(4.00, actualResult, 0.01);
  }
}
