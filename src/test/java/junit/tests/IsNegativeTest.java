package junit.tests;

import junit.BaseTest;
import org.junit.Assert;
import org.junit.Test;

public class IsNegativeTest extends BaseTest {

  @Test
  public void isNegativeTest() {
    boolean result = calculator.isNegative(-2);
    Assert.assertTrue(result);
  }

  @Test
  public void isNegativeFailsTest() {
    boolean result = calculator.isNegative(2);
    Assert.assertFalse(result);
  }
}
