package junit.tests;

import junit.BaseTest;
import org.junit.Assert;
import org.junit.Test;

public class SumTest extends BaseTest {

  @Test
  public void longSumResult() {
    long result = calculator.sum(3L, 2L);
    Assert.assertEquals(5L, result);
  }

  @Test
  public void doubleSumResult() {
    double result = calculator.sum(1.00, 2.00);
    Assert.assertEquals(5.00, result, 0.01);
  }
}
