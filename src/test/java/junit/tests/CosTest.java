package junit.tests;

import junit.BaseTest;
import org.junit.Assert;
import org.junit.Test;

public class CosTest extends BaseTest {

  @Test
  public void cosTest() {
    double result = calculator.cos(30);
    Assert.assertEquals(0.1542, result, 0.0001);
  }
}
