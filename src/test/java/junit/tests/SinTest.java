package junit.tests;

import junit.BaseTest;
import org.junit.Assert;
import org.junit.Test;

public class SinTest extends BaseTest {

  @Test
  public void sinTest() {
    double result = calculator.sin(30);
    Assert.assertEquals(0.500, result, 0.01);
  }
}
