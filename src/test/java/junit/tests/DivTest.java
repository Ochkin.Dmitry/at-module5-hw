package junit.tests;

import junit.BaseTest;
import org.junit.Assert;
import org.junit.Test;

public class DivTest extends BaseTest {

  @Test
  public void divLongTest() {
    long actualResult = calculator.div(2L, 2L);
    Assert.assertEquals(1L, actualResult);
  }

  @Test
  public void divDoubleTest() {
    double actualResult = calculator.div(2.00, 1.00);
    Assert.assertEquals(2.00, actualResult, 0.01);
  }
}
