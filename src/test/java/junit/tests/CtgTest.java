package junit.tests;

import junit.BaseTest;
import org.junit.Assert;
import org.junit.Test;

public class CtgTest extends BaseTest {

  @Test
  public void ctgTest(){
    double result = calculator.ctg(30);
    Assert.assertEquals(-0.1561, result, 0.01);
  }
}
