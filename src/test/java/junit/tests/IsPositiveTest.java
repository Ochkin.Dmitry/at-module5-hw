package junit.tests;

import junit.BaseTest;
import org.junit.Assert;
import org.junit.Test;

public class IsPositiveTest extends BaseTest {

  @Test
  public void isPositiveTest() {
    boolean result = calculator.isPositive(2);
    Assert.assertTrue(result);
  }

  @Test
  public void isPositiveFailsTest() {
    boolean result = calculator.isPositive(-2);
    Assert.assertFalse(result);
  }
}
