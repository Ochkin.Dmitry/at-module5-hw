package junit.tests;

import junit.BaseTest;
import org.junit.Assert;
import org.junit.Test;

public class TgTest extends BaseTest {

  @Test
  public void tgTest() {
    double result = calculator.tg(135);
    Assert.assertEquals(-1, result, 0.01);
  }

  @Test
  public void tgNegativeTest(){
    double result = calculator.tg(30);
    Assert.assertEquals(0.57735, result, 0.01);
  }
}
