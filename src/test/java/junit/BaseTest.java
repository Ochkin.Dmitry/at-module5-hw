package junit;

import com.epam.tat.module4.Calculator;
import org.junit.After;
import org.junit.Before;

public class BaseTest {

  public Calculator calculator;

  @Before
  public void setUpTest() {
    this.calculator = new Calculator();
  }

  @After
  public void tearDown() {
    this.calculator = null;
  }
}
