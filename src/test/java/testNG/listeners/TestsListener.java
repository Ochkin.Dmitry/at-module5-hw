package testNG.listeners;

import java.util.ArrayList;
import java.util.List;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class TestsListener implements ITestListener {

  private List<String> failedTests;

  @Override
  public void onTestStart(ITestResult iTestResult) {
    System.out.println("Test started: " + iTestResult.getMethod().getMethodName() + "\n");
  }

  @Override
  public void onTestSuccess(ITestResult iTestResult) {
    System.out.println("Result: " + iTestResult.getMethod().getMethodName() + ": PASSED" + "\n");
  }

  @Override
  public void onTestFailure(ITestResult iTestResult) {
    String failedTest = "Result: " + iTestResult.getMethod().getMethodName() + ": FAILED";
    failedTests.add(failedTest);
    System.out.println(failedTest + "\n");
  }

  @Override
  public void onTestSkipped(ITestResult iTestResult) {}

  @Override
  public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {}

  @Override
  public void onStart(ITestContext iTestContext) {
    System.out.println("Thread :" + Thread.currentThread().getName());
    failedTests = new ArrayList<>();
  }

  @Override
  public void onFinish(ITestContext iTestContext) {
    System.out.println("======================== TESTS FAILED");
    for (String failedTest : failedTests) {
      System.out.println(failedTest);
    }
  }
}
