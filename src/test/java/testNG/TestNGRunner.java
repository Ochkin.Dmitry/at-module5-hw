package testNG;

import java.util.ArrayList;
import java.util.List;

import org.testng.TestNG;
import org.testng.xml.XmlSuite;

public class TestNGRunner {

  public static void main(String[] args) {
    TestNG testNG = new TestNG();

    XmlSuite suite = new XmlSuite();
    List<String> suitesPaths = new ArrayList<>();
    suitesPaths.add("src/main/resources/paralleled-tests.xml");
    suite.setSuiteFiles(suitesPaths);
    suite.setParallel(XmlSuite.ParallelMode.TESTS);
    suite.setThreadCount(3);

    List<XmlSuite> suites = new ArrayList<>();
    suites.add(suite);

    testNG.setXmlSuites(suites);
    testNG.run();
  }
}
