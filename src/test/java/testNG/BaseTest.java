package testNG;

import com.epam.tat.module4.Calculator;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;

public class BaseTest {

  public Calculator calculator;

  @BeforeClass
  public void setUp() {
    this.calculator = new Calculator();
  }


  @AfterClass
  public void tearDown() {
    this.calculator = null;
  }
}
