package testNG.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import testNG.BaseTest;

public class SqrtTest extends BaseTest {

  @Test
  public void sqrtTest() {
    double result = calculator.sqrt(4.00);
    Assert.assertEquals(2.00, result, 0.01);
  }
}
