package testNG.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import testNG.BaseTest;

public class CtgTest extends BaseTest {

  @Test
  public void ctgTest(){
    double result = calculator.ctg(30);
    Assert.assertEquals(-0.1561, result, 0.01);
  }
}
