package testNG.tests;

import com.epam.tat.module4.Calculator;
import org.testng.Assert;
import org.testng.annotations.Test;
import testNG.BaseTest;

public class DivTest extends BaseTest {

  @Test(groups = "smoke")
  public void divLongTest() {
    Calculator calculator = new Calculator();
    long actualResult = calculator.div(2L, 2L);
    Assert.assertEquals(1L, actualResult);
  }

  @Test
  public void divDoubleTest() {
    double actualResult = calculator.div(2.00, 1.00);
    Assert.assertEquals(2.00, actualResult, 0.01);
  }
}
