package testNG.tests;

import com.epam.tat.module4.Calculator;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testNG.BaseTest;

public class SumTest extends BaseTest {

  @Test(groups = "smoke")
  public void doubleSumResult() {
    Calculator calculator = new Calculator();
    double result = calculator.sum(1.00, 2.00);
    Assert.assertEquals(5.00, result, 0.01);
  }

  @Test(dataProvider = "testData")
  public void longSumResult(long a, long b, long expectedResult) {
    long actualResult = calculator.sum(a, b);
    Assert.assertEquals(actualResult, expectedResult);
  }

  @DataProvider(name = "testData")
  public Object[][] valuesForSum() {
    return new Object[][] {
      {2, 5, 7},
      {-5, 5, 0},
      {-5, 0, -5},
    };
  }
}
