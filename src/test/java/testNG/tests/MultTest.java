package testNG.tests;

import com.epam.tat.module4.Calculator;
import org.testng.Assert;
import org.testng.annotations.Test;
import testNG.BaseTest;

public class MultTest extends BaseTest {

  @Test(groups = "smoke")
  public void multLongTest() {
    Calculator calculator = new Calculator();
    long actualResult = calculator.mult(2L, 2L);
    Assert.assertEquals(4L, actualResult);
  }

  @Test
  public void multDoubleTest() {
    double actualResult = calculator.mult(2.00, 2.00);
    Assert.assertEquals(4.00, actualResult, 0.01);
  }
}
