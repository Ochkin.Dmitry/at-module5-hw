package testNG.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import testNG.BaseTest;


public class CosTest extends BaseTest {

  @Test
  public void cosTest() {
    double result = calculator.cos(30);
    Assert.assertEquals(0.1542, result, 0.0001);
  }
}
