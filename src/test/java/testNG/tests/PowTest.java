package testNG.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import testNG.BaseTest;

public class PowTest extends BaseTest {

  @Test
  public void powTest() {
    double result = calculator.pow(2.00, 2.00);
    Assert.assertEquals(4.00, result, 0.01);
  }
}
