package testNG.tests;

import com.epam.tat.module4.Calculator;
import org.testng.Assert;
import org.testng.annotations.Test;
import testNG.BaseTest;

public class SubTest extends BaseTest {

  @Test(groups = "smoke")
  public void subLongTest() {
    Calculator calculator = new Calculator();
    long actualResult = calculator.sub(2L, 1L);
    Assert.assertEquals(1L, actualResult);
  }

  @Test
  public void subDoubleTest() {
    double actualResult = calculator.sub(3.00, 1.00);
    Assert.assertEquals(2.00, actualResult, 0.01);
  }
}
