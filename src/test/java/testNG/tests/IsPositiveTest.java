package testNG.tests;

import com.epam.tat.module4.Calculator;
import org.testng.Assert;
import org.testng.annotations.Test;
import testNG.BaseTest;

public class IsPositiveTest extends BaseTest {

  @Test(groups = "smoke")
  public void isPositiveTest() {
    Calculator calculator = new Calculator();
    boolean result = calculator.isPositive(2);
    Assert.assertTrue(result);
  }

  @Test
  public void isPositiveFailsTest() {
    boolean result = calculator.isPositive(-2);
    Assert.assertFalse(result);
  }
}
