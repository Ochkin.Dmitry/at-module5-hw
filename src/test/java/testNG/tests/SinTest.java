package testNG.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import testNG.BaseTest;

public class SinTest extends BaseTest {

  @Test
  public void sinTest() {
    double result = calculator.sin(30);
    Assert.assertEquals(0.500, result, 0.01);
  }
}
