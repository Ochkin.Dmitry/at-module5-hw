package testNG.tests;

import com.epam.tat.module4.Calculator;
import org.testng.Assert;
import org.testng.annotations.Test;
import testNG.BaseTest;

public class IsNegativeTest extends BaseTest {

  @Test(groups = "smoke")
  public void isNegativeTest() {
    Calculator calculator = new Calculator();
    boolean result = calculator.isNegative(-2);
    Assert.assertTrue(result);
  }

  @Test
  public void isNegativeFailsTest() {
    boolean result = calculator.isNegative(2);
    Assert.assertFalse(result);
  }
}
